---
title: "Resume"
date: 2018-09-12T16:11:18-07:00
draft: false
---


## Graham L. Brown
### Vancouver, BC V6R 2G1

<table>
<tr><td>Email</td><td><a href="mailto:graham@grellyd.com">graham@grellyd.com</a><br></td></tr>
<tr><td>Website</td><td><a href="https://grellyd.com">https://grellyd.com</a><br></td></tr>
<tr><td>Github</td><td><a href="https://github.com/grellyd">https://github.com/grellyd</a><br></td></tr>
<tr><td>LinkedIn</td><td><a href="https://linkedin.com/in/grellyd">https://linkedin.com/in/grellyd</a><br></td></tr>
<tr><td>Twitter</td><td><a href="https://twitter.com/grellyd">https://twitter.com/grellyd</a><br></td></tr>
</table>


## Objective
<b>Backend</b> software developer for a small to medium innovative company in a <b>network/APi</b> engineering role using a modern language.


## Employment

<table> 
<tr><td colspan=2><h3>Lendesk: Software Developer Co-op</h3></td></tr>
<tr><td>2017<br>Vancouver, BC</td>
<td><ul>
<li>Created a <b>real-time</b> identity verification system of Licensed Mortgage
Brokers in Canada for the Level by Lendesk mobile app in <b>Ruby</b> using
Nokogiri<b> and the core Lendesk <b>Rails</b> monolith.</li>
<li>Led an effort to create a <b>naive bayes</b> based automatic document
classification system in <b>Python</b> using <b>SciKit Learn</b> trained on
Lendesk’s dataset. </li>
</ul>
</td></tr>

<tr><td colspan=2><h3>FINCAD: Software Developer Co-op</h3></td></tr>
<tr><td>2015<br>Surrey, BC</td>
<td><ul>
<li>Authored a <b>Technical Design Document</b> (TDD) for a power user
usability enhancement project after <b>gathering requirements</b> from
multiple stakeholders. </li>
<li>Battled <b>WinForms</b> and <b>VBA</b> to add functionality and enhancements to
the main FINCAD product.</li>
<li>Developed a <b>Django</b> web interface to <b>Twisted</b> asynchronous workers
for dispatching calculation jobs.</li>
</ul>
</td></tr>
</table>

## Projects

<table>
<tr><td colspan=2><h3>Kubo & grellyd.com</h3></td></tr>
<tr><td>2018-Present</td>
<td>
<ul>
<li>Authored and updated my static page website generated with <b>Hugo</b>
using my <b>Kubo</b> template and a custom <b>Go</b> server.</li>
<li>Attended the <b>GoNorthwest conference </b>due to involvement in and
admiration of the Go <b>community</b>.</li>
<li><a href="https://github.com/grellyd/grellyddotcom">https://github.com/grellyd/grellyddotcom</a></li>
<li><a href="https://github.com/grellyd/kubo_hugo">https://github.com/grellyd/kubo_hugo</a></li>
</ul>
</td>
</tr>
<tr><td colspan=2><h3>CPSC416: BlockArt and DistributedDiary</h3></td></tr>
<tr><td>2018</td>
<td>
<ul>
<li>Constructed two <b>Go distributed systems</b> in a <b>team</b> of five students.</li>
<li>Authored a custom <b>Go</b> logging package to track distributed behaviour.</li>
<li><a href="https://github.com/grellyd/cpsc_416_p1_blockchain_art">https://github.com/grellyd/cpsc_416_p1_blockchain_art</a></li>
<li><a href="https://github.com/grellyd/cpsc_416_p2_distributed_diary">https://github.com/grellyd/cpsc_416_p2_distributed_diary</a></li>
</td></li>
</ul>
</tr>
<tr><td colspan=2><h3>SAM: Ericsson Innovation Awards 2017 Semi-Finalist</h3></td></tr>
<tr>
<td>2017</td>
<td>
<ul>
<li>Developed and validated the concept of providing <b>low-cost</b> data-based
farming via satellite imagery analysis for developing communities.</li> 
<li><a href="https://www.ericsson.com/careers/blog/career-topics/gunjana/2017-ericsson-innovation-awards-semi-finalists/">https://www.ericsson.com/careers/blog/career-topics/gunjana/2017-ericsson-innovation-awards-semi-finalists/</a></li>
</ul>
</td>
</tr>
<tr><td colspan=2><h3>Poolr: Vancouver Startup Weekend Hackathon Winner 2016</h3></td></tr>
<tr>
<td>2016-2017</td>
<td> 
<ul>
<li>Pitched our B2B intra-company relationship building app to first place with a ReactJS frontend and a <b>Ruby on Rails</b> backend.</li>
<li>Built <b>mobile</b> <b>MVP</b> alongside Two Tall Totems development studio</li>
<li><a href="https://www.twotalltotems.com/blog/index.php/2016/09/27/vancouver-startup-week-hackathon-and-a-secret-visit/">https://www.twotalltotems.com/blog/index.php/2016/09/27/vancouver-startup-week-hackathon-and-a-secret-visit/</a></li>
</ul>
</td>
</tr>
<tr><td colspan=2><h3>Code the Change: Cofounder of UBC Club building software for Nonprofits</h3></td></tr>
<tr>
<td>2016-2017</td>
<td>
<ul>
<li>Built software solutions for needs in the local <b>nonprofit</b> community</li>
<li>Created the club's website in <b>Ruby on Rails</b></li>
<li><a href="http://codethechange.ca/">http://codethechange.ca/</a></li>
</ul>
</td></tr></table>


## Education

<b>BSc</b>. in <b>Computer Science</b> from the University of British Columbia (UBC).
Graduation November 2018.

## Personal

Avid camper, hiker, cyclist, and sunset aficionado. Equally likely to be coding
in a hammock on the beach as camping next to a mountain stream.

## References

Available upon request


[PDF](/files/graham_l_brown_resume.pdf)
