# Hi I'm Graham...
{{< figure src="/images/ski_profile.jpg" alt="Grellyd Backcountry Skiing in Whistler" class="profile_image" >}}

...a backend developer, backpacker, and wannabe outdoorsman from Vancouver, Canada.

You can find me as **@grellyd** most places online.

This site is a perpetual work in progress. For now, take a look at my [resume](/resume).

The best way to contact me is to reach out via email [graham@grellyd.com](mailto:graham@grellyd.com)
